﻿#include <iostream>
#include <ctime>

int main()
{
	time_t now = time(0);

	tm* ltm = localtime(&now);

	//Позаолит вывести точное текущее время на экран со всеми подробностями
	std::cout << "Year: " << 1900 + ltm->tm_year << std::endl; //Текущий год
	std::cout << "Month: " << 1 + ltm->tm_mon << std::endl; //Текущий месяц
	std::cout << "Day: " << ltm->tm_mday << std::endl; // Текущий день
	std::cout << "Time: " << ltm->tm_hour << ":"; //Текущее время составляется из час:минуты:секунды
	std::cout << ltm->tm_min << ":";
	std::cout << ltm->tm_sec << std::endl;
	std::cout << "\n";

	int CurrentTime = ltm->tm_mday; //Задаем текущее число календаря переменной

	const int n = 2;
	int Massive[n][n]{ {0,1}, {1,2} }; //Задаем массив по принципу [i][j] = i+j (счет i и j начинается с 0)

	for (int i = 0; i < n; i++) //Выводим на экран весь массив
	{
		for (int j = 0; j < n; j++)
		{
			std::cout << Massive[i][j];
		}
		std::cout << "\n";
	}
	std::cout << "\n";

	int NecessaryIndex = CurrentTime % n; //Считаем остаток от деления день/размерность массива
	std::cout << "Necessary index is: " << NecessaryIndex << std::endl;
	std::cout << "\n";

	std::cout << Massive[NecessaryIndex][0] + Massive[NecessaryIndex][1] << std::endl;
}